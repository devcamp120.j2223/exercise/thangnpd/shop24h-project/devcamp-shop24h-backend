const express = require('express');
const app = express();
const port = 8000;
const path = require('path');

//Import mongoose
var mongoose = require('mongoose');
var uri = "mongodb://localhost:27017/CRUD_Shop24h";

// Cấu hình để app đọc được body request dạng json
app.use(express.json());

// Cấu hình để app được được tiếng Việt UTF8
app.use(express.urlencoded({
    extended: true
}))

//Import Model để sinh ra DB
const productTypeModel = require('./app/models/productTypeModel');
const productModel = require('./app/models/productModel');
const customerModel = require("./app/models/customerModel");
const orderModel = require('./app/models/orderModel');

//Import router
const productTypeRouter = require('./app/routers/productTypeRouters');
const productRouter = require('./app/routers/productRouters');
const customerRouter = require('./app/routers/customerRouters');
const orderRouter = require('./app/routers/orderRouters');

app.use('/', productTypeRouter); // tất cả request về productType đều chạy ở router này
app.use('/', productRouter); // tất cả request về productType đều chạy ở router này
app.use('/', customerRouter); // tất cả request về productType đều chạy ở router này
app.use('/', orderRouter); // tất cả request về productType đều chạy ở router này

//Connect DB
mongoose.connect(uri, function (error) {
	if (error) throw error;
	console.log('MongoDB Successfully connected');
});

app.listen(port, () => {
  console.log(`Shop24h app listening on port ${port}`);
})