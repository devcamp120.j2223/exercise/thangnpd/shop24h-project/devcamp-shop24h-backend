//Import router
const express = require('express');
const router = express.Router();

//Import Controller
const { createCustomer, 
  getAllCustomer, 
  getCustomerById,
  updateCustomerById,
  deleteCustomerById
} = require('../controllers/customerControllers');

//Create new product type
router.post('/customers', createCustomer);

//Get all product type
router.get('/customers', getAllCustomer);

//Get 1 product type by ID
router.get('/customers/:customerId', getCustomerById);

//Update 1 product type by ID
router.put('/customers/:customerId', updateCustomerById);

//Delete 1 product type by ID
router.delete('/customers/:customerId', deleteCustomerById);

module.exports = router; //Export router để call main app ở file app.js