//Import router
const express = require('express');
const router = express.Router();

//Import Controller
const { createOrderOfCustomer, 
  getAllOrder, 
  getAllOrderOfCustomer,
  getOrderById,
  updateOrder,
  deleteOrder
} = require('../controllers/orderControllers');

//Create new order of customer
router.post('/customers/:customerId/orders', createOrderOfCustomer);

//Get all order of customer
router.get('/customers/:customerId/orders/', getAllOrderOfCustomer);

//Get all order
router.get('/orders', getAllOrder);

//Get 1 order by ID
router.get('/orders/:orderId', getOrderById);

//Update 1 order by ID
router.put('/orders/:orderId', updateOrder);

//Delete 1 order by ID
router.delete('/orders/:orderId', deleteOrder);

module.exports = router; //Export router để call main app ở file app.js