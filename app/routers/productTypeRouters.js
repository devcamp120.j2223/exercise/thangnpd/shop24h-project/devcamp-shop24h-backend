//Import router
const express = require('express');
const router = express.Router();

//Import Controller
const { createProductType, 
  getAllProductType, 
  getProductTypeById,
  updateProductTypeById,
  deleteProductTypeById
} = require('../controllers/productTypeControllers');

//Create new product type
router.post('/product-types', createProductType);

//Get all product type
router.get('/product-types', getAllProductType);

//Get 1 product type by ID
router.get('/product-types/:productTypeId', getProductTypeById);

//Update 1 product type by ID
router.put('/product-types/:productTypeId', updateProductTypeById);

//Delete 1 product type by ID
router.delete('/product-types/:productTypeId', deleteProductTypeById);

module.exports = router; //Export router để call main app ở file app.js