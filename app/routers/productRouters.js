//Import router
const express = require('express');
const router = express.Router();

//Import Controller
const { createProduct, 
  getAllProduct, 
  getProductById,
  updateProductById,
  deleteProductById
} = require('../controllers/productControllers');

//Create new product type
router.post('/products', createProduct);

//Get all product type
router.get('/products', getAllProduct);

//Get 1 product type by ID
router.get('/products/:productId', getProductById);

//Update 1 product type by ID
router.put('/products/:productId', updateProductById);

//Delete 1 product type by ID
router.delete('/products/:productId', deleteProductById);

module.exports = router; //Export router để call main app ở file app.js