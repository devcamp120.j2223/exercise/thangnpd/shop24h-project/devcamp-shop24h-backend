//Import customer model vào để tương tác với model
const customerModel = require('../models/customerModel');

//Khai báo thư viện mongoose để tạo _id cho mỗi record mới
var mongoose = require('mongoose');

const createCustomer = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  console.log(bodyRequest);

  // B2: Kiểm tra dữ liệu
  let createCustomer = bodyRequest;
  if (!bodyRequest.fullName) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "fullName is required"
    })
  }
  if (!bodyRequest.phone) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "phone is required"
    })
  }
  if (!bodyRequest.email) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "email is required"
    })
  }
  // if (!mongoose.Types.ObjectId.isValid(bodyRequest.orders)) {
  //   response.status(400).json({
  //     status: "Error 400: Bad Request",
  //     message: "orders is not valid"
  //   })
  // }

  //Add _id vào object sẽ tạo trong DB
  createCustomer._id = mongoose.Types.ObjectId();

  // B3: Thao tác với cơ sở dữ liệu
  customerModel.create(createCustomer, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(201).json({
        status: "Success: Customer created",
        data: data
      })
    }
  })
}

const getAllCustomer = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  customerModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all customers success",
        dataSize: data.length,
        data: data
      })
    }
  })
}

const getCustomerById  = (request, response) => {
   //B1: Chuẩn bị dữ liệu
   let customerId = request.params.customerId;
   console.log(customerId);
   //B2: Validate dữ liệu
   if (!mongoose.Types.ObjectId.isValid(customerId)) {
     response.status(400).json({
       status: "Error 400: Bad Request",
       message: "customerId  is not valid"
     })
   }
   //B3: Thao tác với cơ sở dữ liệu
   customerModel.findById(customerId, (error, data) => {
     if (error) {
       response.status(500).json({
         status: "Error 500: Internal server error",
         message: error.message
       })
     } else {
       response.status(200).json({
         status: "Success: Get customer by ID success",
         data: data
       })
     }
   })
}

const updateCustomerById  = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let customerId = request.params.customerId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "customerId is not valid"
    })
  }

  //B3: Thao tác với cơ sở dữ liệu
  let customerUpdate = {
    fullName: bodyRequest.fullName,
    phone: bodyRequest.phone,
    email: bodyRequest.email
  }
  customerModel.findByIdAndUpdate(customerId, customerUpdate, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update customer success",
        data: data
      })
    }
  })
}

const deleteCustomerById  = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let customerId = request.params.customerId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "customerId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  customerModel.findByIdAndDelete(customerId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(204).json({
        status: "Success: Delete customer success"
      })
    }
  })
}

module.exports = {
  createCustomer, 
  getAllCustomer, 
  getCustomerById,
  updateCustomerById,
  deleteCustomerById
}