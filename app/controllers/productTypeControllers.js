//Import course model vào để tương tác với model
const productTypeModel = require('../models/productTypeModel');

//Khai báo thư viện mongoose để tạo _id cho mỗi record mới
var mongoose = require('mongoose');

const createProductType = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  console.log(bodyRequest);

  // B2: Kiểm tra dữ liệu
  let createProductType = bodyRequest;
  if (!bodyRequest.name) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "name is required"
    })
  }

  //Add _id vào object sẽ tạo trong DB
  createProductType._id = mongoose.Types.ObjectId();

  // B3: Thao tác với cơ sở dữ liệu
  productTypeModel.create(bodyRequest, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(201).json({
        status: "Success: Product Type created",
        data: data
      })
    }
  })
}

const getAllProductType = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  productTypeModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all product type success",
        dataSize: data.length,
        data: data
      })
    }
  })
}

const getProductTypeById  = (request, response) => {
   //B1: Chuẩn bị dữ liệu
   let productTypeId = request.params.productTypeId;
   console.log(productTypeId);
   //B2: Validate dữ liệu
   if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
     response.status(400).json({
       status: "Error 400: Bad Request",
       message: "productTypeId  is not valid"
     })
   }
   //B3: Thao tác với cơ sở dữ liệu
   productTypeModel.findById(productTypeId, (error, data) => {
     if (error) {
       response.status(500).json({
         status: "Error 500: Internal server error",
         message: error.message
       })
     } else {
       response.status(200).json({
         status: "Success: Get productType by ID success",
         data: data
       })
     }
   })
}

const updateProductTypeById  = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let productTypeId = request.params.productTypeId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "productTypeId is not valid"
    })
  }

  //B3: Thao tác với cơ sở dữ liệu
  let productTypeUpdate = {
    name: bodyRequest.name,
    description: bodyRequest.description
  }
  productTypeModel.findByIdAndUpdate(productTypeId, productTypeUpdate, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update productType success",
        data: data
      })
    }
  })
}

const deleteProductTypeById  = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let productTypeId = request.params.productTypeId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "productTypeId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  productTypeModel.findByIdAndDelete(productTypeId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(204).json({
        status: "Success: Delete productType success"
      })
    }
  })
}

module.exports = {
  createProductType, 
  getAllProductType, 
  getProductTypeById,
  updateProductTypeById,
  deleteProductTypeById
}