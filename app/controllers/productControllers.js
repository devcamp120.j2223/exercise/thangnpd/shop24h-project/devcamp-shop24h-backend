//Import course model vào để tương tác với model
const productModel = require('../models/productModel');

//Khai báo thư viện mongoose để tạo _id cho mỗi record mới
var mongoose = require('mongoose');

const createProduct = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  console.log(bodyRequest);

  // B2: Kiểm tra dữ liệu
  let createProduct = bodyRequest;
  if (!bodyRequest.name) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "name is required"
    })
  }
  if (!mongoose.Types.ObjectId.isValid(bodyRequest.type)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "type is not valid"
    })
  }
  if (!bodyRequest.imageUrl) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "imageUrl is required"
    })
  }
  if (!bodyRequest.buyPrice) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "buyPrice is required"
    })
  }
  if (!bodyRequest.promotionPrice) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "promotionPrice is required"
    })
  }

  //Add _id vào object sẽ tạo trong DB
  createProduct._id = mongoose.Types.ObjectId();

  // B3: Thao tác với cơ sở dữ liệu
  productModel.create(createProduct, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(201).json({
        status: "Success: Product created",
        data: data
      })
    }
  })
}

const getAllProduct = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  productModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all product success",
        dataSize: data.length,
        data: data
      })
    }
  })
}

const getProductById  = (request, response) => {
   //B1: Chuẩn bị dữ liệu
   let productId = request.params.productId;
   console.log(productId);
   //B2: Validate dữ liệu
   if (!mongoose.Types.ObjectId.isValid(productId)) {
     response.status(400).json({
       status: "Error 400: Bad Request",
       message: "productId  is not valid"
     })
   }
   //B3: Thao tác với cơ sở dữ liệu
   productModel.findById(productId, (error, data) => {
     if (error) {
       response.status(500).json({
         status: "Error 500: Internal server error",
         message: error.message
       })
     } else {
       response.status(200).json({
         status: "Success: Get product by ID success",
         data: data
       })
     }
   })
}

const updateProductById  = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let productId = request.params.productId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "productId is not valid"
    })
  }

  //B3: Thao tác với cơ sở dữ liệu
  let productUpdate = {
    name: bodyRequest.name,
    description: bodyRequest.description
  }
  productModel.findByIdAndUpdate(productId, productUpdate, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update product success",
        data: data
      })
    }
  })
}

const deleteProductById  = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let productId = request.params.productId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "productId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  productModel.findByIdAndDelete(productId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(204).json({
        status: "Success: Delete product success"
      })
    }
  })
}

module.exports = {
  createProduct, 
  getAllProduct, 
  getProductById,
  updateProductById,
  deleteProductById
}