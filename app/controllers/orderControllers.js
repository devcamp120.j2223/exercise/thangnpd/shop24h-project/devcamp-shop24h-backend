//Import order model vào để tương tác với model
const orderModel = require('../models/orderModel');

//Import customer model vào để tương tác với model
const customerModel = require('../models/customerModel');

//Khai báo thư viện mongoose để tạo _id cho mỗi record mới
var mongoose = require('mongoose');

const createOrderOfCustomer = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  let customerId = request.params.customerId;
  console.log(bodyRequest);

  // B2: Kiểm tra dữ liệu
  let createOrderOfCustomer = bodyRequest;


  //Add _id vào object sẽ tạo trong DB
  createOrderOfCustomer._id = mongoose.Types.ObjectId();

  // B3: Thao tác với cơ sở dữ liệu
  orderModel.create(createOrderOfCustomer, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      customerModel.findByIdAndUpdate(customerId,
        {
          $push: { orders: data._id }
        },
        (err, updatedCustomer) => {
          if (err) {
            return response.status(500).json({
              status: "Error 500: Internal server error",
              message: err.message
            })
          } else {
            return response.status(201).json({
              status: "Success: Order created",
              data: data
            })
          }
        })
    }
  })
}

const getAllOrder = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  orderModel.find((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get all order success",
        dataSize: data.length,
        data: data
      })
    }
  })
}

const getOrderById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let orderId = request.params.orderId;
  console.log(orderId);
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "orderId  is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  orderModel.findById(orderId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get order by ID success",
        data: data
      })
    }
  })
}

const updateOrder = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let orderId = request.params.orderId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "orderId is not valid"
    })
  }

  //B3: Thao tác với cơ sở dữ liệu
  let orderUpdate = {
    note: bodyRequest.note,
    cost: bodyRequest.cost
  }
  orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Update order success",
        data: data
      })
    }
  })
}

const deleteOrder = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let orderId = request.params.orderId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "orderId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  orderModel.findByIdAndDelete(orderId, (error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Delete order success"
      })
    }
  })
}

const getAllOrderOfCustomer = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let customerId = request.params.customerId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    response.status(400).json({
      status: "Error 400: Bad Request",
      message: "customerId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  customerModel.findById(customerId).populate('orders').exec((error, data) => {
    if (error) {
      response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      response.status(200).json({
        status: "Success: Get orders of customer",
        dataSize: data.orders.length,
        data: data.orders
      })
    }
  })
}

module.exports = {
  createOrderOfCustomer,
  getAllOrder,
  getAllOrderOfCustomer,
  getOrderById,
  updateOrder,
  deleteOrder
}