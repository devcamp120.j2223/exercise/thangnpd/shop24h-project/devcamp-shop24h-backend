//Import mongoose and schema
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const customerSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId
  },
  fullName: {
    type: String,
    require: true
  },
  phone: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  address: {
    type: String,
    default: "address"
  },
  city: {
    type: String,
    default: "no city"
  },
  country: {
    type: String,
    default: "VN"
  },
  orders: [
    {
      type: mongoose.Types.ObjectId,
      ref: "order"
    }
  ],
  ngayTao: {
    type: Date,
    default: Date.now()
  },
  ngayCapNhat: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("customer", customerSchema)