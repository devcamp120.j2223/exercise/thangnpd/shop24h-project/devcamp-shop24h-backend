//Import mongoose and schema
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productTypeSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId
  },
  name: {
    type: String, 
    unique: true, 
    require: true
  },
  description: {
    type: String
  },
	ngayTao: {
    type: Date, 
    default: Date.now()
  },
  ngayCapNhat: {
    type: Date, 
    default: Date.now()
  }
});

module.exports = mongoose.model("producttype", productTypeSchema)