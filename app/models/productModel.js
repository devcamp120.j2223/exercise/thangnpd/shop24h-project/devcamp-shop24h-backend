//Import mongoose and schema
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId
  },
  name: {
    type: String, 
    unique: true, 
    require: true
  },
  description: {
    type: String
  },
  type: {
    type: mongoose.Types.ObjectId, 
    ref: "producttype", 
    required: true
  },
	imageUrl: {
    type:String, 
    required: true
  },
	buyPrice: {
    type: Number, 
    required: true
  },
	promotionPrice: {
    type: Number, 
    required: true
  },
	amount: {
    type:Number, 
    default: 0
  },
	ngayTao: {
    type: Date, 
    default: Date.now()
  },
  ngayCapNhat: {
    type: Date, 
    default: Date.now()
  }
});

module.exports = mongoose.model("product", productSchema)