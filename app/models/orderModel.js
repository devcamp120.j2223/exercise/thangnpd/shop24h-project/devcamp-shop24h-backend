//Import mongoose and schema
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId
  },
  orderDate: {
    type: Date, 
    default: Date.now()
  }, 
  shippedDate: {
    type: Date, 
    default: Date.now()
  },
  note: {
    type: String
  },
  orderDetail: {
    type:Array
  },
  cost: {
    type: Number, 
    default: 0
  },
	ngayTao: {
    type: Date, 
    default: Date.now()
  },
  ngayCapNhat: {
    type: Date, 
    default: Date.now()
  }
});

module.exports = mongoose.model("order", orderSchema)